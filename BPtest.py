# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
C:\python\p\WinPython-32bit-2.7.5.3\settings\.spyder2\.temp.py
"""
from __future__ import division
import numpy as np

def initial(L, verbose=False):
    '''
    Generates the initial code, for the X and Z components and the initial 
    syndromes,it also generates the initial error maps
    returns:2*L x 2*L array (code),2*L x 2*L array (code),2*L x 2*L array (code),
    L x L array (syndrome),L x L array (syndrome)
    '''
    code = np.zeros((2*L,2*L))
    code[::2,1::2] = 1
    code[1::2,::2] = 1#The code is generated to resemble the toric code
    codeX = 1*code
    codeZ = 1*code
    syndromeX = np.ones((L,L))
    syndromeZ = np.ones((L,L))
    if verbose==True:
        print code
    return code, codeX,codeZ,syndromeX,syndromeZ

def applyerror(codeX,codeZ,synX,synZ,i,j,error):
    '''
    For a qubit in a given position [i,j],it applies an error type 'error' in 
    the code 'code' and in the syndrome 'syn'
    arg: 2L X 2L code,2L X 2L code, L x L syndrome,L x L syndrome, position, string indicating type of error
    returns: 2L X 2L array(code), L x L array (syndrome),2L X 2L array(code), L x L array (syndrome)
    '''
    L = np.shape(codeX)[0]//2
    if error == 'X' or error == 'Y':#Since the position of the qubits are different depending 
    #on if the row number is even or odd,both cases need to be coded separately
        codeX[i,j]=-codeX[i,j]
        if i%2==0:
            synZ[((i-1)//2)%L,     j//2] = -synZ[((i-1)//2)    %L,j//2]
            synZ[(((i-1)//2)+1)%L, j//2] = -synZ[(((i-1)//2)+1)%L,j//2]
        if i%2==1:
            synZ[i//2, ((j-1)//2)    %L] = -synZ[i//2,((j-1)//2)    %L]
            synZ[i//2,(((j-1)//2)+1) %L] = -synZ[i//2,(((j-1)//2)+1)%L]
    if error == 'Z' or error == 'Y':
        codeZ[i,j]=-codeZ[i,j]
        if i%2==0:
            synX[i//2, ((j-1)//2)    %L] = -synX[i//2,((j-1)//2)    %L]
            synX[i//2,(((j-1)//2)+1) %L] = -synX[i//2,(((j-1)//2)+1)%L]
        if i%2==1:
            synX[ ((i-1)//2)   %L, j//2]  = -synX[((i-1)//2)    %L,j//2]
            synX[(((i-1)//2)+1)%L, j//2]  = -synX[(((i-1)//2)+1)%L,j//2]
    return [codeX,codeZ,synX,synZ]
  
def Beliefpropagation(p_err,code,syndrome,syntype,cycles):
    '''
    Performs Belief propagation on the code by using the syndromes given 
    and the probabilty of error. For this function it is assumed that the 
    probability of error is the same for all the qubits and for all types of
    error.
    
    arg: float (probability of error happening), 2L x 2L array (Code),
    L x L array (syndrome), string indicating type of error, integer
    returns: L x L array(belief for syndromes), 2L x 2L array (belief for errors),
    2L x 2L array (code)
    '''
    L = np.shape(code)[0]//2
    l = np.log10((1-p_err)/p_err)#The prior belief for the errors is calculated
    mu_e_s = np.zeros((2*L,2*L,2))#The belief for error to syndrome is generated
    #and with an initial condition that all elements are equal to 0
    mu_s_e = np.zeros((L,L,4))#The belief for syndrome to error is generated
    #and with an initial condition that all elements are equal to 0
    pred  = np.zeros((2*L,2*L))
    check = np.zeros((2*L,2*L),dtype=int)#A matrix which keeps track of which qubits
    #have been used
    mu_e = np.zeros((2*L,2*L))#The error belief for the last layer of the Belief
    #propagation algorithm
    for t in range(cycles):
        check = np.zeros((2*L,2*L),dtype=int)
        for i in range(L):#Each cycle is divided in two processes, the first one
          #obtains the belief from the syndrome to the errors by using the belief
          #from the errors to the syndrome
            for j in range(L):
                for it in range(4):#Depending on the qubit, we will use a different
                  #set of qubits to calculate the syndrome belief
                    step = 1
                    if syntype=='Z':#Depending on the type of error, each syndrome is
                      #determined by a different set of qubits
                        if it!=0:
                            step = np.tanh((mu_e_s[(2*i)  %(2*L),(1+2*j)%(2*L),
                                                   check[(2*i)  %(2*L),(1+2*j)%(2*L)]])/2)*step
                            check[(2*i)  %(2*L),(1+2*j)%(2*L)] = 1
                        if it!=3:
                            step = np.tanh((mu_e_s[(2*i+1)%(2*L),    2*j%(2*L),
                                                   check[(2*i+1)%(2*L),    2*j%(2*L)]])/2)*step
                            check[(2*i+1)%(2*L),    2*j%(2*L)] = 1
                        if it!=1:
                            step = np.tanh((mu_e_s[(2*i+1)%(2*L),(2+2*j)%(2*L),
                                                   check[(2*i+1)%(2*L),(2+2*j)%(2*L)]])/2)*step
                            check[(2*i+1)%(2*L),(2+2*j)%(2*L)] = 1
                        if it!=2:
                            step = np.tanh((mu_e_s[(2*i+2)%(2*L),(2*j+1)%(2*L),
                                                   check[(2*i+2)%(2*L),(2*j+1)%(2*L)]])/2)*step
                            check[(2*i+2)%(2*L),(2*j+1)%(2*L)] = 1
                    if syntype=='X':
                        if it!=1:
                            step = np.tanh((mu_e_s[(2*i)  %(2*L),(2*j+1)%(2*L),
                                                   check[(2*i)  %(2*L),(2*j+1)%(2*L)]])/2)*step
                            check[(2*i)  %(2*L),(2*j+1)%(2*L)] = 1
                        if it!=2:
                            step = np.tanh((mu_e_s[(2*i+1)%(2*L),    2*j%(2*L),
                                                   check[(2*i+1)%(2*L),    2*j%(2*L)]])/2)*step
                            check[(2*i+1)%(2*L),    2*j%(2*L)] = 1 
                        if it!=0:
                            step = np.tanh((mu_e_s[(2*i-1)%(2*L),    2*j%(2*L),
                                                   check[(2*i-1)%(2*L),    2*j%(2*L)]])/2)*step
                            check[(2*i-1)%(2*L),    2*j%(2*L)] = 1
                        if it!=3:
                            step = np.tanh((mu_e_s[(2*i)  %(2*L),(2*j-1)%(2*L),
                                                   check[(2*i)  %(2*L),(2*j-1)%(2*L)]])/2)*step
                            check[(2*i)  %(2*L),(2*j-1)%(2*L)] = 1
                    mu_s_e[i,j,it] = (-1)**(1//2 - syndrome[i,j]//2)*2*np.arctanh(step)
        for i in range(2*L):
            for j in range(2*L):#The second one obtains the belief from 
              #the errors to the syndrome by using the belief
              #from the syndromes to the errors.
                if code[i,j]!=0:
                    if syntype=='Z':#Depending on the type of error, each map
                      #of qubits is determined by a different set of elements 
                      #of the syndrome
                        if i%2==0:
                            mu_e_s[i,j,1] = mu_s_e[  ((i-1)//2)  %L, j//2,2] + l
                            mu_e_s[i,j,0] = mu_s_e[(((i-1)//2)+1)%L, j//2,0] + l
                        if i%2==1:
                            mu_e_s[i,j,1] = mu_s_e[i//2 , ((j-1)//2)   %L,1] + l
                            mu_e_s[i,j,0] = mu_s_e[i//2,(((j-1)//2)+1) %L,3] + l
                    if syntype=='X':
                        if i%2==0:
                            mu_e_s[i,j,1] = mu_s_e[i//2 ,((j-1)//2)    %L,1] + l
                            mu_e_s[i,j,0] = mu_s_e[i//2,(((j-1)//2)+1) %L,3] + l
                        if i%2==1:
                            mu_e_s[i,j,0] = mu_s_e[ ((i-1)//2)   %L, j//2,2] + l
                            mu_e_s[i,j,1] = mu_s_e[(((i-1)//2)+1)%L, j//2,0] + l
    if t == (cycles-1):#For the last layer, we will sum all the combinations of
      #the syndrome belief to obtain the final error belief
        for i in range(2*L):
            for j in range(2*L):#The second one obtains the belief from 
              #the errors to the syndrome by using the belief
              #from the syndromes to the errors.
                if code[i,j] != 0:
                    mu_e[i,j] = l
                    if syntype=='Z':#Depending on the type of error, each map
                      #of qubits is determined by a different set of elements 
                      #of the syndrome
                        if i%2==0:
                            #mu_e[i,j] += mu_s_e[  ((i-1)//2)   %L, j//2,0] 
                            mu_e[i,j] += mu_s_e[(((i-1)//2)+1) %L, j//2,0]
                            mu_e[i,j] += mu_s_e[  ((i-1)//2)   %L, j//2,2]
                            #mu_e[i,j] += mu_s_e[(((i-1)//2)+1) %L, j//2,2]
                        if i%2==1:
                            #mu_e[i,j] += mu_s_e[i//2 , ((j-1)//2)    %L,3]
                            mu_e[i,j] += mu_s_e[i//2 ,(((j-1)//2)+1) %L,3]
                            mu_e[i,j] += mu_s_e[i//2 , ((j-1)//2)    %L,1] 
                            #mu_e[i,j] += mu_s_e[i//2 ,(((j-1)//2)+1) %L,1]
                    if syntype=='X':
                        if i%2==0:
                            #mu_e[i,j] += mu_s_e[i//2,((j-1)//2)    %L,0] 
                            mu_e[i,j] += mu_s_e[i//2,(((j-1)//2)+1)%L,3]
                            mu_e[i,j] += mu_s_e[i//2,((j-1)//2)    %L,1] 
                            #mu_e[i,j] += mu_s_e[i//2,(((j-1)//2)+1)%L,2]
                        if i%2==1:
                            #mu_e[i,j] += mu_s_e[ ((i-1)//2)   %L, j//2,3] 
                            mu_e[i,j] += mu_s_e[(((i-1)//2)+1)%L, j//2,0] 
                            mu_e[i,j] += mu_s_e[ ((i-1)//2)   %L, j//2,2] 
                            #mu_e[i,j] += mu_s_e[(((i-1)//2)+1)%L, j//2,1]
    for i in range(2*L):
        for j in range(2*L):
            if code[i,j]!=0:
                errcheck =1/(1+np.exp(mu_e[i,j]))#We can obtain the probaility 
                #of not having an error by using Prob = 1/(1+exp(belief)), 
                #then we will guess if the qubit has an error or not by 
                #checking which probability is higher, the probability of 
                #having an error or the probability of not having an error
                if errcheck <= 0.5:
                    pred[i,j] =  1
                else:
                    pred[i,j] = -1
    return[mu_s_e,mu_e,pred]            
    
#Syndrome and error mapping, given error 
L=8#The dimensions of the code given by the number of plaquettes
num_err = 5#the number of errors the program will introduce into the code
p_err = 0.01#The probability of having an error
code, codeX,codeZ,syndromeX,syndromeZ = initial(L)
print('Errorless code')
print(code)
[codeX,codeZ,syndromeX,syndromeZ]=applyerror(codeX,codeZ,syndromeX,syndromeZ,2,1,'Y')
[codeX,codeZ,syndromeX,syndromeZ]=applyerror(codeX,codeZ,syndromeX,syndromeZ,2,11,'Y')
print('Code Z')
print(codeZ)
print('Syndrome X')
print(syndromeX)
#Belief propagation
[mu_s_e,mu_e,pred] = Beliefpropagation(p_err,code,syndromeX,'X',12)
print('Syndrome Belief')
print(mu_s_e[:,:,0])
print('Error Belief')
print(mu_e)
print('Error prediction')
print(pred)
print('Check')
print(np.array_equal(pred,codeZ))